from askbot.models import Group
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse

def join_group(request):
    if request.user.is_authenticated():
        group = Group.objects.get(id=401)
        request.user.join_group(group, force=True)
        message = _('Welcome to the WaterAid Partners group')
    else:
        message = _('Please first log in or register an account. '
            'Once you have registered, you will need '
            'to click on the invitation link again'
        )
    request.user.message_set.create(message=message)
    return HttpResponseRedirect(reverse('index'))
