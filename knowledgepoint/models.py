"""models for the Knowledgepoint askbot extensions"""
from askbot.models import GroupMembership
from askbot.models import Post
from askbot.models import Thread
from askbot.models import User
from askbot.models.signals import new_question_posted
from askbot.models.signals import question_edited
from askbot.models.signals import group_membership_changed
from django.db import models
from django_countries.fields import CountryField
import logging

class ThreadData(models.Model):
    thread = models.ForeignKey(Thread)
    country = CountryField(blank=True) 

def save_thread_data(question=None, form_data=None, **kwargs):
    if 'country' in form_data:
        data = ThreadData(
                thread=question.thread,
                country=form_data['country']
            )
        data.save()

def log_new_question_info(question=None, **kwargs):
    """Log posting of new questions, in the attempt to track
    unwanted posting of questions through partner sites"""
    question = Post.objects.get(id=question.id)
    thread = question.thread
    spaces = thread.spaces.all()

    space_names = [space.name for space in spaces]
    space_names = ', '.join(space_names)
    title = thread.title

    groups = thread.groups.all()
    group_names = [group.name for group in groups]
    group_names = ', '.join(group_names)

    data = (question.id, space_names, group_names, title)
    logging.critical('New question: id=%d spaces="%s" groups="%s" title=%s' % data)

def update_thread_data(question=None, form_data=None, **kwargs):
    data, created = ThreadData.objects.get_or_create(thread=question.thread)
    data.country = form_data['country']
    data.save()

def autoapprove_wateraid_members(user=None, group=None,
                                level=None, action=None, **kwargs):
    if action == 'add':
        if group.name == 'WaterAid':
            if level == GroupMembership.FULL:
                if user.status == 'w':
                    User.objects.filter(pk=user.pk).update(status='a')


new_question_posted.connect(save_thread_data)
new_question_posted.connect(log_new_question_info)
question_edited.connect(update_thread_data)
group_membership_changed.connect(autoapprove_wateraid_members)
