��    \      �     �      �  C   �          "  )   6  Q   `  `   �     	     	     6	     E	     ^	     s	     �	     �	     �	     �	     �	     �	     �	     �	      
  
   
     
  :   6
  	   q
     {
     �
     �
     �
     �
     �
  	   �
     �
     �
  	             &     2     ?     O     V  f   q  	   �     �     �     �  /   �     .     :     C  )   L     v     �     �     �     �  (   �     �     �     �     
          3     F     K     X     ^     l  -   u  1   �  
   �     �     �     �  	                  "  
   /     :     C     P  
   X     c     q  (   v  
   �     �  !   �     �     �  �  �  W   �     �     �  4   �  Q   4  `   �     �     �       "   1     T  $   i     �     �     �     �     �     �  #   �          *     I  (   _  Q   �     �     �  &   �  !        2     L     T  	   m     w     {     �     �     �     �     �     
       z   .     �     �     �     �  2   �       
   &     1  =   A          �     �     �     �  /   �     �     	          2  !   E     g     y     ~     �     �  	   �  9   �  G   �     =     I     Q     Y  	   m     w  	   �     �     �  	   �     �     �     �     �       3        S     e     x     �     �               [   U       3   .          C   @              2       \             W   G       (             0                4   :   '   T              #   Y       5      7      S   L   Z                                   )          -   N   ?   I       6   %   J   8              	      1       *            R   &      <       
       E   D   A   P       V       +       F               =   $       M   ;   ,   "   O       /   !      Q             >   X   B   H   K       9         - to expand, or dig in by adding more tags and revising the query.  or  (cannot be changed) (only one answer per question is allowed) <span class="count">%(cnt)s</span> Vote <span class="count">%(cnt)s</span> Votes  <span class="count">%(counter)s</span> Question <span class="count">%(counter)s</span> Questions Add details Add details (optional) Answer Answers Answer Your Own Question Answered anonymously Ask %(group_name)s Ask Your Question Ask a question Can moderate site Cancel Change language Details Edit Your Previous Answer Edit question Edit user profile Group info Hi there! Please sign in If you feel you can contribute an answer, please go ahead! Languages Like Login/Signup to Answer Login/Signup to Post My primary language is: Options Question - in one sentence Questions RSS Related country (optional) Save edit Screen Name Search tip: Search tips: Select language Tagged This response is published To see questions in other languages, <a class="langs-setup" href="%(langs_url)s">please click here</a> Undo Like Update Updated User profile What do you do? What are you trying to achieve? Your answer activity add logo add tags and a query to focus your search answer answers back badges change logo change picture comments and answers to others questions current number of votes delete logo edit description edit profile email subscription settings followed questions help hide preview inbox including you liked by list of email addresses of pre-approved users mark this answer as correct (click again to undo) moderation network overview people & groups questions related country remove reset author reset tags settings show preview sort by start over subscriptions tags this answer has been selected as correct thumb down thumb up user has voted up this many times users votes Project-Id-Version: knowledgepoint
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-25 03:15-0500
PO-Revision-Date: 2014-09-25 08:11+0000
Last-Translator: evgeny <evgeny.fadeev@gmail.com>
Language-Team: French (http://www.transifex.com/projects/p/knowledgepoint/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
  - pour développer ou restreindre en ajoutant plus de tags et en révisant la requête ou (ne peut pas être changer) Limiter une réponse par question et par utilisateur <span class="count">%(cnt)s</span> Vote <span class="count">%(cnt)s</span> Votes  <span class="count">%(counter)s</span> Question <span class="count">%(counter)s</span> Questions Ajouter des détails Ajouter du détail (optionnel) Résponse Résponses  Répondre à votre propre question répondu anonymement poser une question à %(group_name)s Poser votre question Poser une question  Peut modérer le site Annuler Changer de langue Détails Éditer votre précédente réponse Modifier une question Modifier le profil utilisateur Information du groupe Bonjour, s'il vous plaît connectez-vous Si vous sentez que vous pouvez contribuer une réponse, s'il vous plaît allez-y! Langues J'aime S'identifier/S'inscrire pour répondre S'identifier/s'aboner pour poster Ma langue principale est: Options Question - en une phrase Questions RSS Pays associés (en option) Enregistrer les modifications Pseudo Conseil pour la recherche: Conseils pour la recherche: Sélectionner la langue Marqué Cette réponse est publiée Por voir les questions dans d'autres langues, <a class="langs-setup" href="%(langs_url)s">s'il vous plaît cliquez ici</a> annuler Mettre à jour actualisée Profil de l'utilisateur Que faire? Qu'est-ce que vous essayez d'atteindre? Votre réponse actualité ajouter un logo ajouter des tags et une requête pour affiner votre recherche résponse résponses Retour badges changer le logo changer d'image Commentaires et réponses à d'autres questions Nombre de votes actuel supprimer le logo Éditer la description Modifier le profil Réglages d'abonnement aux emails questions suivies aide Masquer l'aperçu boite de réception compris vous aimé par liste des adresses email des utilisateurs pré-approuvés marquer cette réponse comme correcte (cliquez à nouveau pour annuler) Modération réseau aperçu personnes & groupes questions Pay associé supprimer Réinitialiser l'auteur Réinitialiser les éttiquettes réglages Afficher l'aperçu Trier par » Recommencer Abonnements aux emails Étiquettes Cette réponse a été sélectionnée comme correct pouce vers le bas pouce vers le haut pouce vers le haut utilisateurs/ votes 